/*	Author: Lucas Anesti
	Description: Implementation of the Perceptron Learning Algorithm
				 and solutions to Problem 1.4 and Exercise 1.4 from the book:

				 "Learning from Data" by Yaser S. Abu-Mostafa
										 Malik Magdon-Ismail
										 Hsuan-Tien Lin
*/

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <sstream>
#include <cstdlib>
#include <vector>
#include <algorithm>	// fill_n, generate
#include <ctime>		// time
#include <cstdlib> 		// srand, rand
#include <cmath>
#include <numeric>		// inner_product


// function object to generate random float values within a domain
class randFloat
{
private:
	float domainLow;
	float domainHigh;
public:
	randFloat(float low, float high)
			 : domainLow( low ), domainHigh( high ) {}

	float operator()()
	{
		/* multiplying rand() by the domain width d generates a float
		 * that is larger than RAND_MAX by a factor d, allowing
		 * output values greater than 1.0. */
		float d = domainHigh - domainLow; 
		float r = domainLow + ( rand() * d ) / (static_cast<float>(RAND_MAX));
		return r;
	}
};


class Perceptron
{
private:
	// x,y pairs, where x is a set of floats and y is -1.0 or 1.0
	std::vector< std::pair< std::vector<float>, float >> data;
	// target function f used to generate data
	std::vector<float> f;
	// the most recent set of weights = optimal function g, which is
	// an approximation to target fxn f
	std::vector<float> g;	
 	// history of the weights: w(0), w(1), ..., w(t), w(t+1), ...
	std::vector< std::vector<float> > w;
	// count the total number of loops learning goes through
	size_t loopCount;
	// count the total number of updates to g made by learn
	size_t iterations;
	// number of samples correctly classified by g compared to f
	size_t numCorrect;

public:
	// d is dimension as in R^d
	// since x0 is always 1, it is not considered a dimension so we add
	// +1 to dimensions entered by user
	Perceptron(size_t d = 0) : loopCount(0), iterations(0), numCorrect(0)
	{ 
		// 2 dimensional hypotheses requires additional term: bias ( w0 )
		g.resize( d+1 );
		f.resize( d+1 );
		fill_n(g.begin(), d+1, 0.0);
		fill_n(f.begin(), d+1, 0.0);
		w.push_back( g );
	}

	void generateData( size_t dataSize, float domainLow = -100.0, 
									 float domainHigh = 100.0 )
	{	
		if(dataSize == 0) return;

		//random float generating functor
		randFloat getRandFloat( domainLow, domainHigh );
		
		// target function f
		generate(f.begin(), f.end(), getRandFloat );

		for( int i = 0; i < dataSize; ++i)
		{
			std::vector<float> x( g.size() );
			x[0] = 1.0;
			generate( x.begin()+1, x.end(), getRandFloat );
			float sum = inner_product( f.begin(), f.end(), x.begin() , 0.0);
			float y = ( sum > 0.0 ? 1.0 : -1.0 );
			// std::cout << "sum = " << sum << " " << y << "\n";

			data.push_back( std::make_pair( x, y ));
		}	
	}


	// learning algorithm that selects a misclassified data point by 
	// searching through the data set sequentially. It starts over every
	// time a misclassified point is found.
	void learn()
	{
		if(g.size() == 0 ) return;
	
		bool complete = false;
		bool misclassified = false;
		while(! complete )
		{
			complete = true;
			misclassified = false;
			int index = 0;
			while( index < data.size() && ! misclassified  )
			{
				auto x = data[index].first;
				auto y = data[index].second;
				float sum = inner_product(g.begin(), g.end(), x.begin(), 0.0);
				
				/*	If the sum = 0, we classify the result as false because
				 	a point on the line is not exactly classified.
					This is addressed in part a) of Exercise 1.3:

						y(t)*w(t)*x(t) < 0

					Since x(t) is misclassified by w(t), then y(t), the real
					answer, and x(t*)w(t), the classification of the current 
					input, will have different signs, since g(x) incorrect.
					A negative and a positive number multiplied gives a 
					negative answer. But this only depends on w not being 0,
					which according to the book, should be the initial values
					for w.
				*/
				bool diffSign = (sum * y <= 0.0);		

				if( diffSign )
				{
					/*	Based on the book, w(t+1) is the new weight vector we
						calculate after we adjust the weight for each point.
						So, t tracks the sequence of corrections made that
						only happen when data is misclassified.
					*/
					// The initial g = <0,0,...> is pushed onto w in the
					// constructor, so w.size > 0
					size_t t = w.size() - 1;
					w.push_back( std::vector<float>( g.size(), 0.0) );
					for(int i = 0; i < g.size(); ++i)
					{
						/*	Formula 1.4: The only difference is that the 
							book's formula adds vectors directly.
						
								w(t+1) = w(t) + y(t)*x(t)
						*/
						w[t+1][i] = w[t][i] + y*x[i];
					}
					g = w[t+1];

					/*	We have found at least one misclassified sample.
						The line will change, so it may misclassify other 
						points in the process. Run the loop one more time.
					*/
					complete = false;
					misclassified = true;
					++iterations;
				}
				++index;
			}
			++loopCount;
		}
	}


	/*** Avoid this algorithm as it takes longer to converge.  ***/

	// learning algorithm that selects a misclassified data point by 
	// searching through the data set sequentially. It iterates
	// though the entire set every time a misclassified point is found
	void learnFullLoop()
	{
		bool complete = false;
		while(! complete )
		{
			complete = true;
			for(auto& d : data)
			{
				auto x = d.first;
				auto y = d.second;
				float sum = inner_product(g.begin(), g.end(), x.begin(), 0.0);
				
				/*	If the sum = 0, we classify the result as false because
				 	a point on the line is not exactly classified.
					This is addressed in part a) of Exercise 1.3:

						y(t)*w(t)*x(t) < 0

					Since x(t) is misclassified by w(t), then y(t), the real
					answer, and x(t*)w(t), the classification of the current 
					input, will have different signs, since g(x) incorrect.
					A negative and a positive number multiplied gives a 
					negative answer. But this only depends on w not being 0,
					which according to the book, should be the initial values
					for w.
				*/
				bool diffSign = (sum * y <= 0.0);		

				if( diffSign )
				{
					/*	Based on the book, w(t+1) is the new weight vector we
						calculate after we adjust the weight for each point.
						So, t tracks the sequence of corrections made that
						only happen when data is misclassified.
					*/
					// The initial g = <0,0,...> is pushed onto w in the
					// constructor, so w.size > 0
					size_t t = w.size() - 1;
					w.push_back( std::vector<float>( g.size(), 0.0) );
					for(int i = 0; i < g.size(); ++i)
					{
						/*	Formula 1.4: The only difference is that the 
							book's formula adds vectors directly.
						
								w(t+1) = w(t) + y(t)*x(t)
						*/
						w[t+1][i] = w[t][i] + y*x[i];
					}
					g = w[t+1];

					/*	We have found at least one misclassified sample.
						The line will change, so it may misclassify other 
						points in the process. Run the loop one more time.
					*/
					complete = false;
					++iterations;
				}
			}
			++loopCount;
		}
	}

	// learning algorithm that selects a misclassified data point 
	// randomly through the set of known misclassified points
	void learnRand()
	{
		// a temporary vector to hold the indeces of misclassified data 
		// points from which we will choose a random point
		std::vector<int> misclassified;

		bool complete = false;
		while(! complete )
		{
			complete = true;
			// collect all the misclassified data points( using indeces only)
			for(int i = 0; i < data.size(); ++i)
			{
				auto x = data[i].first;
				auto y = data[i].second;
				float sum = inner_product(g.begin(), g.end(), x.begin(), 0.0);
				
				bool diffSign = (sum * y <= 0.0);		

				if( diffSign )
				{
					misclassified.push_back(i);
				}
			}

			complete = (misclassified.size() == 0);
			if( ! complete )
			{
				// pick a random misclassified data point
				int index = misclassified[ rand() % misclassified.size() ];
				auto d = data[index];
				auto x = d.first;
				auto y = d.second;

				// The initial g = <0,0,...> is pushed onto w in the
				// constructor, so w.size > 0
				int t = w.size() - 1;
				w.push_back( std::vector<float>( g.size(), 0.0) );
				for(int i = 0; i < g.size(); ++i)
				{
					/*	Formula 1.4: The only difference is that the 
						book's formula adds vectors directly.
					
							w(t+1) = w(t) + y(t)*x(t)
					*/
					w[t+1][i] = w[t][i] + y*x[i];
				}
				g = w[t+1];

				++iterations;
			}
			misclassified.clear();
			++loopCount;
		}
	}



	void showData()
	{
		std::cout << std::fixed << std::setprecision(2);
		const int xWidth = 11;
		const int displayWidth = 25;

		std::cout << "\n";
		for(int i = 0; i < g.size(); ++i)
		{
			std::cout << std::setw( xWidth ) 
					  << std::left << "x"+std::to_string(i);
		}
		std::cout << std::setw(xWidth) << "f(x)";
		std::cout << std::setw(xWidth) << "g(x)" << "\n";
		// std::cout << std::string( (g.size()+1) * xWidth, '-');
		std::cout << std::string( (g.size()+1) * xWidth + 5, '-') << "\n";

		for( auto d : data )
		{
			for( auto x : d.first )
			{
				std::cout << std::setw ( xWidth ) << x;
			}
			float s = inner_product(g.begin(), g.end(), d.first.begin(), 0.0);
			float gx = ( (s > 0.0) ? 1.0 : -1.0 );
			numCorrect += (gx == d.second) ? 1 : 0; 
			std::cout << std::setw( xWidth )<< d.second;
			std::cout << std::setw( xWidth )<< s << "\n";
		}
	}

	void showHistory()
	{
		std::cout << "History: " << std::endl;
		for( auto t : w)
		{
			std::cout << "    ( ";
			for( auto i : t)
			{
				std::cout << i << ", ";
			}
			std::cout << ")" << std::endl;
		}	
	}

	void showResult(std::string algo = "")
	{

		std::cout << "\nResult for algorithm:" << algo << "\n";
		std::cout <<   "---------------------" 
				  << std::string(algo.size()+1, '-') << "\n";

		numCorrect = 0;
		std::vector<int> incorrectIndexes;

		std::cout << std::fixed << std::setprecision(2);
		const int displayWidth = 25;

		int index = 0;
		for( auto d : data )
		{
			float s = inner_product(g.begin(), g.end(), d.first.begin(), 0.0);
			float gx = ( (s > 0.0) ? 1.0 : -1.0 );
			if( gx == d.second)
			{
				//numCorrect += (gx == d.second) ? 1 : 0; 
				++numCorrect;
			}
			else
			{
				incorrectIndexes.push_back(index);
			}
			++index;
		}

		std::cout << std::setw(displayWidth) << std::left 
				  << "Target fxn f " << " : ";
		for( int i = 0; i < f.size(); ++i)
		{
			float display = (f[i] != 0.0) ? f[i] : 0.0;
			std::cout << std::setw(6) << display << "*x"<< i;
			if( i != f.size()-1 )
			{
				std::cout << "  +  ";
			}
		}	
		std::cout << std::endl;

		std::cout << std::setw(displayWidth) << std::left 
				  << "Hypothesis g " << " : ";
		for( int i = 0; i < g.size(); ++i)
		{
			float display = (g[i] != 0.0) ? g[i] : 0.0;
			std::cout << std::setw(6) << display << "*x"<< i;
			if( i != g.size()-1 )
			{
				std::cout << "  +  ";
			}
		}	
		std::cout << std::endl;

		std::cout << std::setw(displayWidth) << "Learning completed after " 
				  << " : " <<  iterations << " iterations" << "\n";
		std::cout << std::setw(displayWidth) << "Learning completed after " 
				  << " : " <<  loopCount << " loops" << "\n";

		std::cout << std::setw(displayWidth) << "Total correct "
				  << " : " << numCorrect << " / " << data.size() << "\n";
		for( auto i : incorrectIndexes ) std::cout << i << ", ";
		std::cout << "\n";
	}


	void reset()
	{
		fill_n( g.begin(), g.size(), 0.0 );
		// w.clear();
		loopCount = 0;
		iterations = 0;
		numCorrect = 0;
	}

	void clear()
	{
		data.clear();
		reset();
	}

	int getIterations()
	{
		return iterations;
	}

	// method to plot the data, target function, and hypothesis
	// for 2 dimensional space
	void plot()
	{
		if( g.size() == 3)
		{	
			std::string filePos = "dataPos.dat";
			std::string fileNeg = "dataNeg.dat";
			std::ofstream dataPos( filePos, std::ios::out );
			std::ofstream dataNeg( fileNeg, std::ios::out );
			if( !dataPos || !dataNeg )
			{
				std::cerr << "File " << filePos << " or " 
						  << fileNeg << " could not be opened. \n";

				exit(1);
			}

			for( auto& d : data )
			{
				if( d.second > 0 )
				{
					for ( auto& x : d.first )
					{
						dataPos << x << " ";
					}
					dataPos << "\n";
				}
				else
				{
					for ( auto& x : d.first )
					{
						dataNeg << x << " ";
					}
					dataNeg << "\n";
				}
			}

			dataPos.close();
			dataNeg.close();

			
			auto y = g;
			//solve for x2 ( = y )
			for( auto& i : y ) i /= g[2];
			for( auto& i : y ) i *= -1;
			std::string hyp = std::to_string(y[1]) + "*x + " + std::to_string(y[0]);

			auto targetF = f;
			for( auto& i : targetF ) i /= f[2];
			for( auto& i : targetF ) i *= -1;
			std::string targ = std::to_string(targetF[1]) + "*x + " + std::to_string(targetF[0]);

			std::cout << "Plotting : \n";
			std::cout << "\tg(x) = " << y[1] << "*x + " << y[0] << "\n"; 
			std::cout << "\tf(x) = " << targetF[1] << "*x + " << targetF[0] << "\n"; 

			std::string showTarget = targ + " title 'f(x)=" + targ + "'";
			std::string showHyp    = hyp  + " title 'g(x)=" + hyp  + "'";
			system( ("gnuplot -p -e \"  plot 'dataPos.dat' using 2:3 title 'Pos' lc rgb 'green',  \
										'dataNeg.dat' using 2:3 title 'Neg' lc rgb 'red' , "
										+ showTarget + " lc rgb 'gray' , "
										+ showHyp + " lc rgb 'black' \" ").c_str() );

		}
		else
		{
			std::cout << "Too many dimensions to plot.\n";
		}
	}
};



int main()
{
	// line width for max output
	const int lineWidth = 60;

	srand( time(0) );
	int dim = 2;
	int datasize = 20;



/*** Exercise 1.4 ***/
	std::cout << "\n\n" << std::string(20,'=') << " Exercise 1.4 "
			  << std::string(20,'=')<< "\n";

	Perceptron p( dim );
	p.generateData( datasize );
	p.learn();
	p.showResult("");



/*** Problem 1.4  ***/
	std::cout << "\n\n\n" <<  std::string(20,'=') << " Problem 1.4 "
			  << std::string(20,'=')<< "\n";

// 1.4 a,b
	std::cout << "\n" << std::string(20,'_') << " Part a,b"
			  << ", Dim = " << dim << " , Size = "<< datasize << " : \n";
	Perceptron pa( dim );
	pa.generateData( datasize );
	pa.learn();
	pa.showResult();


// 1.4 b
	pa.plot();



// 1.4 c,d,e
	std::vector<std::string> parts {"c", "d", "e"};
	std::vector<int> datasizes {20, 100, 1000};
	for( int i = 0;  i < datasizes.size(); ++i )
	{
		std::cout << "\n" << std::string(20,'_') << " Part " << parts[i]
				  << ", Dim = 2, Size = " << datasizes[i] << " : \n" ;
		Perceptron pc( dim );
		pc.generateData( datasizes[i] );
		pc.learn();
		pc.showResult();

	}

// 1.4 f
	std::cout << "\n" << std::string(20,'_') << " Part f"
			  << ", Dim = " << dim << " , Size = "<< datasize << " : \n";
	dim = 10;
	datasize = 1000;
	Perceptron pf( dim );
	pf.generateData( datasize );
	pf.learn();
	pf.showResult();

// 1.4 g, using both algorithms for performance comparison
	std::cout << "\n" << std::string(20,'_') << " Part f"
			  << ", Dim = " << dim << " , Size = "<< datasize << " : \n";
	dim = 2;
	datasize = 100;
	Perceptron pg( dim );
	pg.generateData( datasize );
	pg.learn();
	pg.showResult("Sequential");
	pg.plot();


	pg.reset();
	pg.learnRand();
	pg.showResult("Random");
	pg.plot();


	pg.clear();

	int trials = 50;
	int rnd = 0;
	int seq = 0;
	int randVictories = 0;
	for(int i = 0; i < trials; ++i)
	{
		pg.generateData(100);

		pg.learnRand();
		rnd = pg.getIterations();
		pg.reset();

		pg.learn();
		seq = pg.getIterations();
		if( rnd < seq )
		{ 
			++randVictories;	
		}
		
		pg.clear();
	}

	std::cout << "\nRandom algo was faster in : " << randVictories << " / " 
			  << trials << " trials.\n";




	return EXIT_SUCCESS;
}
