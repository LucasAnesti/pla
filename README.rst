===
PLA
===
* Perceptron Learning Algorithm
* Author: Lucas Anesti
* Date: March 2, 2019

Purpose:
--------
Implementation of the Perceptron Learning Algorithm and solutions to 
Problem 1.4 and Exercise 1.4 from the book: 

*Learning from Data* by Yaser S. Abu-Mostafa, Malik Magdon-Ismail, Hsuan-Tien Lin



System and Tools:
-----------------
* Ubuntu 18.04.1 LTS
* GNU Make 4.1
* g++ 7.3.0
* C++11
* git version 2.17.1
* gnuplot 5.2


To use:
-------
* **make**: Compile source files and generate an executable.
* **make run**: Run the program.
* **make clean**: Remove the executable and other project files.
